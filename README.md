# asl-fall19-project
Git repository structure:
* src/ch/ethz folder contains all the .java classes used to represent the middleware. Makefile and build.xml generate a middleware-mstevan.jar file using these classes. 
*  experiments_scripts folder contains all bash scripts for running all the experiments on azure. After executing these scripts, all log files will be in ~/experiment_results/version_GUID folder. They can then be coppied to experiment_result and uploaded to git. This folder also contains scripts that automatically run all the python scripts in the experiment_parsing. 
*  experiments_results folder contains all the logs from middleware, memtier clients and memcached servers. These are the source files for python scripts.
*  experiments_parsing folder contains all the python scripts used for parsing. They produce files and plots at their path in the filesystem. These are the source files for report.

To summarize in short: 
* Files in experiments_scripts generate files in experiments_results. 
* Files in experiments_results are the source for the experiments_parsing. 
* Files in experiments_parsing are the source for report.

experiments_scripts structure:
* configuration script is populated once on startup of all servers on azure as we use single same deployment to run all the experiments. It is used by all experiment_X script from which IP addresses and DNS names are obtained for the deployment.
* experiment_X. For each section X in (2.1, 2.3, 3.1, 3.2, 3.3, 3.4) we have a specific script to be able to run experiments. Difference between these scripts is that each starts with the same table as its corresponding experiment table presented in the project description. Each script produces subfolder experiment_X with logs that will be explained in experiment_results folder.
* iperf script runs iperf command between all pairs of servers available in configuration script and produces iperf subfolder with logs that will be explained in experiment_results folder.
* populate_server is helper script used within experiment_X to repopulate memcached server with different value size.
* run_memtier_client is helper script used within experiment_X to run memtier client and send requests to the desired server (middleware).
* startup-start_stop_vms is used to start and stop all servers in azure.
* run_everything runs iperf and all experiment_X scripts where X in (2.1, 2.3, 3.1, 3.2, 3.3, 3.4)
* parse_memtier runs automatically all the python scripts in experiment_parsing/experiment_X folder dedicated for parsing memtier logs
* parse_mws runs automatically all the python scripts in experiment_parsing/experiment_X folder dedicated for parsing middleware logs
* parse_everything runs parse_memtier and parse_mw script for all experiment_X folders in experiment_parsing where X in (2.1, 2.3, 3.1, 3.2, 3.3, 3.4)

experiment_results structure:
* iperf folder. Between each pair of servers on the azure, we run iperf command to check maximum network bandwidth and produce logs. These logs are in form X-to-Y where X and Y are in (client_1, client_2, client_3, server_1, server_2, server_3, mw_1, mw_2) 
* experiment_X folder. For each section in setup without middleware X in (2.1, 2.2) we have a folder. Each contains subfolders with logs from memtier clients and memcached servers.
    * client_X subfolder. Experiments in all corresponding sections are ran with 3 clients so X is in (1, 2, 3). These subfolders contain logs produced by corresponding servers on azure by running the memtier client. Logs are in form: vs-Y_vcpr_Z-server_Q-rep_W where Y is in (64, 256, 512, 1024) and corresponds to value size, Z is in (4, 8, 16, 32) and corresponds to virtual clients per thread, Q is in (1, 2, 3) and corresponds to the memcached server which memtier client is sending requests to, W is in (1, 2, 3, 4) and corresponds to the number of repetitions. Values of Y, Z, Q and W depend on the experiment setup of the corresponding section.
    * server_X subfolder. Value of X is in (1, 2, 3) and it depends on the experiment setup of the corresponding section. These subfolders contain logs produced by corresponding servers on azure by running the memcached server. Logs are in form vs_Y where Y is in (64, 256, 512, 1024) and corresponds to value size because we only repopulate once per each value size. Additionally, it also contains ping and dstat subfolders that contain logs produced by each command. These logs are in form vs_Z-vcpr_Q-rep_W where Z is in (64, 256, 512, 1024) and corresponds to value size, Q is in (4, 8, 16, 32) and corresponds to virtual clients per thread, W is in (1, 2, 3, 4) and corresponds to the number of repetitions. Values of Y, Z, Q and W depend on the experiment setup of the corresponding section.
* experiment_X folder. For each section in setup with middleware X in (3.1, 3.2, 3.3, 3.4) we have a folder. Each contains subfolders with logs from middlewares, memtier clients and memcached servers.
    * client_X subfolder. Experiments in all corresponding sections are ran with 3 clients so X is in (1, 2, 3). These subfolders contain logs produced by corresponding servers on azure by running the memtier client. Logs are in form: vs-Y_vcpr_Z-wtpm_R-mw_Q-rep_W where Y is in (64, 256, 512, 1024) and corresponds to value size, Z is in (4, 8, 16, 32) and corresponds to virtual clients per thread, R is in (8, 32, 64) and corresponds to worker threads per middleware, Q is in (1, 2) and corresponds to the middleware which memtier client is sending requests to, W is in (1, 2, 3, 4) and corresponds to the number of repetitions. Values of Y, Z, R, Q and W depend on the experiment setup of the corresponding section.
    * server_X subfolder. Value of X is in (1, 2, 3) and it depends on the experiment setup of the corresponding section. These subfolders contain logs produced by corresponding servers on azure by running the memcached server. Logs are in form vs_Y where Y is in (64, 256, 512, 1024) and corresponds to value size because we only repopulate once per each value size. Additionally, it also contains ping and dstat subfolders that contain logs produced by each command. These logs are in form vs_Z-vcpr_Q-wtpm_R-rep_W where Z is in (64, 256, 512, 1024) and corresponds to value size, Q is in (4, 8, 16, 32) and corresponds to virtual clients per thread, R is in (8, 32, 64) and corresponds to worker threads per middleware, W is in (1, 2, 3, 4) and corresponds to the number of repetitions. Values of Y, Z, Q and W depend on the experiment setup of the corresponding section. 
    * mw_X subfolder. Value of X is in (1, 2) and it depends on the experiment setup of the corresponding section. These subfolders contain logs produced by corresponding servers on azure by running the middleware. Logs are in form: Summed_Worker_Thread_Counters_vs-Y_vcpr_Z-wtpm_R-rep_W.csv and Middleware_Logs_And_Exceptions_vs-Y_vcpr_Z-wtpm_R-rep_W.txt where Y is in (64, 256, 512, 1024) and corresponds to value size, Z is in (4, 8, 16, 32) and corresponds to virtual clients per thread, R is in (8, 32, 64) and corresponds to worker threads per middleware, W is in (1, 2, 3, 4) and corresponds to the number of repetitions. Values of Y, Z, R and W depend on the experiment setup of the corresponding section. Summed_Worker_Thread_Counters contains logs with all the captured measurements outputted per 1 second. Middleware_Logs_And_Exceptions contains logs other events and exceptions that happened during middleware execution. Additionally, it also contains ping and dstat subfolders that contain logs produced by each command. These logs are in form vs_Z-vcpr_Q-wtpm_R-rep_W.

experiment_parsing structure:
* experiment_X folder. For each section in setup without middleware X in (2.1, 2.2) we have a folder. Each contains python scripts that aggregate logs from memtier clients and produce plots and other required files within the same folder.
    * plotting_tp_nc.py script is used to produce a plot of TP as a function of NumClients. At the same time it produced the file memtier-clients_tp.csv 
    * plotting_tp_vs.py script is used to produce a plot of TP as a function of Value size
    * plotting_rt_nc.py script is used to produce a plot of RT as a function of NumClients. At the same time it produced the file memtier-clients_rt.csv
    * plotting_rt_vs.py script is used to produce a plot of RT as a function of Value size
    * Memtier-clients_rt_tp_manually_merged.csv is file produce manually by myself and merging memtier-clients_rt.csv and memtier-clients_tp.csv in order to calculate data for IL.
* experiment_X folder. For each section in setup with middleware X in (3.1, 3.2, 3.3, 3.4) we have a folder. Each contains python scripts that aggregate logs from both memtier clients and middlewares and produce plots and other required files within the same folder.
    * plotting_tp_nc.py script is used to produce plots of TP as a function of NumClients using memtier client logs in form memtier-clients-wtpm_Y_tp_nc.png. At the same time it produced files in form memtier_clients-wtpm_Y_tp.csv. 
    * plotting_tp_vs.py script is used to produce plots of TP as a function of value size using memtier client logs in form memtier-clients-wtpm_Y_tp_vs.png.
    * plotting_rt_nc.py script is used to produce plots of RT as a function of NumClients using memtier client logs in form memtier-clients-wtpm_Y_rt_nc.png. At the same time it produced files in form memtier_clients-wtpm_Y_rt.csv.
    * plotting_rt_vs.py script is used to produce plots of RT as a function of value size using memtier client logs in form memtier-clients-wtpm_Y_rt_vs.png.
    * plotting_tp_nc_mw.py script is used to produce plots of TP as a function of NumClients using middleware logs in form mw-wtpm_Y_tp_nc.png. At the same time it produced files in form mw-wtpm_Y_tp.csv. 
    * plotting_tp_vs_mw.py script is used to produce plots of TP as a function of value size using middleware logs in form mw-wtpm_Y_tp_vs.png.
    * plotting_rt_nc_mw.py script is used to produce plots of RT as a function of NumClients using middleware logs in form mw-wtpm_Y_rt_nc.png. At the same time it produced files in form mw-wtpm_Y_rt.csv.
    * plotting_rt_vs_mw.py script is used to produce plots of RT as a function of value size using middleware logs in form mw-wtpm_Y_rt_vs.png.
    * interactive_law-wtpm_Y is file produced automatically by create_il_all_section_3_data.py script in order to verify IL and is used in the report. 
    * Y in all the namings is in range (8, 32, 64) and it depends on the experiment setup of the corresponding section.
* create_il_all_section_3_data.py script. For each experiment_X folder where X in (3.1, 3.2, 3.3, 3.4), merges memtier_clients-wtpm_Y_tp.csv, memtier_clients-wtpm_Y_rt.csv, mw-wtpm_Y_tp.csv and mw-wtpm_Y_rt.csv, does required calculations and produces interactive_law-wtpm_Y. Y in all the namings is in range (8, 32, 64) and it depends on the experiment setup of the corresponding section of the experiment_X folder.
* average_dstat_all_section_2_experiments.py. For each section in setup without middleware X in (2.1, 2.2) and experiment_results/experiment_X/server_Y folder aggregates dstat data and outputs it as average_dstat_all_section_2_experiments.csv file that contains rows for each value size, virtual clients per thread combination. Y is in (1, 2, 3) and corresponds to the memcached server which memtier client is sending requests to and it depends on the experiment setup of the corresponding section of the experiment_X folder.
* average_dstat_all_section_3_experiments.py. For each section in setup with middleware X in (3.1, 3.2, 3.3, 3.4), experiment_results/experiment_X/server_Y/dstat folder and experiment_results/experiment_X/mw_Z/dstat aggregates dstat data and outputs it as average_dstat_all_section_3_experiments.csv file that contains rows for each value size, virtual clients per thread, worker threads per middleware combination. Y is in (1, 2, 3) and corresponds to the memcached server which middleware is sending requests to, Z is in (1, 2) and corresponds to the middleware which memtier client is sending requests to and they depend on the experiment setup of the corresponding section of the experiment_X folder.
* average_memtier_all_section_2_experiments.py. For each section in setup without middleware X in (2.1, 2.2) and experiment_results/experiment_X/client_Y folder aggregates memtier clients log data and outputs it as average_memtier_all_section_2_experiments.csv file that contains rows for each value size, virtual clients per thread combination. Y is in (1, 2, 3) and corresponds to the memtier client that is sending requests and it depends on the experiment setup of the corresponding section of the experiment_X folder.
* average_memtier_all_section_3_experiments.py. For each section in setup with middleware X in (3.1, 3.2, 3.3, 3.4), experiment_results/experiment_X/client_Y folder and aggregates memtier clients log data and outputs it as average_memtier_all_section_3_experiments.csv file that contains rows for each value size, virtual clients per thread, worker threads per middleware combination. Y is in (1, 2, 3) and corresponds to the memtier client that is sending requests and it depends on the experiment setup of the corresponding section of the experiment_X folder.
* average_mw_all_section_3_experiments.py. For each section in setup with middleware X in (3.1, 3.2, 3.3, 3.4), experiment_results/experiment_X/mw_Y folder and aggregates middleware log data and outputs it as average_mw_all_section_3_experiments.csv file that contains rows for each value size, virtual clients per thread, worker threads per middleware combination. Y is in (1, 2) and corresponds to the middleware and it depends on the experiment setup of the corresponding section of the experiment_X folder.

report structure:
* 1.2 Middleware Data-Structures
* 1.2.1 - check src/ch/ethz/RunMW.java
* 1.2.2 - check src/ch/ethz/MyMiddleware.java
* 1.2.3 - check src/ch/ethz/Request.java
* 1.2.4 - check src/ch/ethz/WorkerThread.java
* 1.2.5 - check src/ch/ethz/Counter.java
* 1.2.6 - check src/ch/ethz/Logger.java
* 1.2.7 - check src/ch/ethz/Constants.java
* 1.4 Work Balancing - check report/work_balance_proof_vs_1024-vcpr_32-wtpm_64-rep_1.txt
* 1.5 Data Processing - check experiment_parsing folder and it’s structure description
* 1.6 Experimental Setup - check experiment_scripts folder and it’s structure description
* 2 Baseline without Middleware
* 2.X.1 Setup - check experiment_2.X in experiments_scripts folder where X is in (1, 2)
* 2.X.2 Throughput - check memtier-clients_tp_nc.png and memtier-clients_tp_vs.png in experiment_parsing/experiment_2.X folder where X is in (1, 2)
* 2.X.3 Response Time - check memtier-clients_rt_nc.png and memtier-clients_rt_vs.png in experiment_parsing/experiment_2.X folder where X is in (1, 2)
* 2.X.4 Result Analysis - check memtier-clients_rt_tp_manually_merged.csv in experiment_parsing/experiment_2.X folder where X is in (1, 2)
* 2.X.5 Explanation - check average_memtier_all_section_2_experiments.csv and average_dstat_all_section_2_experiments.csv in experiment_parsing folder  where X is in (1, 2)
* 3 Baseline with Middleware
* 3.X.1 Setup - check experiment_3.X in experiments_scripts folder where X is in (1, 2, 3, 4)
* 3.X.2 Throughput - check mw-wtpm_Y_tp_nc.png and mw-wtpm_Y_tp_vs.png in experiment_parsing/experiment_3.X folder where X is in (1, 2, 3, 4) and Y is worker threads per middleware in (8, 32, 64)
* 3.X.3 Response Time - check mw-wtpm_Y_rt_nc.png and mw-wtpm_Y_rt_vs.png in experiment_parsing/experiment_3.X folder where X is in (1, 2, 3, 4) and Y is worker threads per middleware in (8, 32, 64)
* 3.X.4 Result Analysis - check interactive_law-wtpm_Y in experiment_parsing/experiment_3.X folder where X is in (1, 2, 3, 4) and Y is worker threads per middleware in (8, 32, 64)
* 3.X.5 Explanation - check average_mw_all_section_3_experiments.csv and average_dstat_all_section_2_experiments.csv in experiment_parsing folder  where X is in (1, 2, 3, 4)
* 3.5 Summary - 3.1 saturation point - 64 WT, 64 VS, 96 Clients, 3.2 saturation point - 64 WT, 64 VS, 96 Clients, 3.3 saturation point - 64 WT, 64 VS, 96 Clients, 3.4 saturation point - 32 WT, 64 VS, 96 Clients
* 4 2K Analysis
* 4.1 - check 2k_analysis.ods in experiment_parsing folder
* 5 Queueing Model
* 5.1 - check queueing_theory_3.2_from_all_section_3_mm1&mmm.ods
* 5.2 - check queueing_theory_3.2_from_all_section_3_mm1&mmm.ods
* 5.2.2 - use E[n_q]=(1+(w*x)^w/(w!*(1-x))+sum from n=1 to w-1 of (w*x)^n/n!)^-1*((w*x)^w/(w!*(1-x)))*x/(1-x), w=wtpm formula in Wolfram Alpha
* 5.3 - check queueing_theory_3.4_from_all_section_3_network_of_queues.ods