package ch.ethz;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.util.*;
import java.util.concurrent.LinkedBlockingQueue;

public class WorkerThread extends Thread{
    LinkedBlockingQueue<Request> requestQueue;
    private List<SocketChannel> serversChannels;
    private Selector serverSelector;
    private ByteBuffer responseBuffer;
    private int totalBytesRead;
    private Counter counter;
    private int loadBalance;

    public WorkerThread(LinkedBlockingQueue<Request> queue, List<String> mcAddresses) {
        requestQueue = queue;
        serversChannels = new ArrayList<SocketChannel>();
        responseBuffer = ByteBuffer.allocate(Constants.RESPONSE_BUFFER_SIZE);
        counter = new Counter();
        loadBalance = Constants.RANDOM.nextInt(mcAddresses.size());

        try {
            serverSelector = Selector.open();

            for (String serverAddress : mcAddresses) {
                // Parametrized ports instead of Hardcoded 11211
                String[] ip_port = serverAddress.split(":");
                SocketChannel channel = SocketChannel.open(new InetSocketAddress(ip_port[0], Integer.parseInt(ip_port[1])));

                // Multiplexing with serverSelector, more information in SingleGet
                channel.configureBlocking(false);
                serversChannels.add(channel);
                channel.register(serverSelector, SelectionKey.OP_READ);
            }
        }
        catch (IOException e) {
            Constants.MIDDLEWARE_LOGS_AND_EXCEPTIONS_SB.append("EXCEPTION: IO in WorkerThread.ctor()" + e.getMessage() + "\n");
            e.printStackTrace();
            System.exit(-1);
        }

        this.start();
    }

    @Override
    public void run() {
        while (Constants.IS_MW_WORKING) {
            try {
                Request request;
                // Take is atomic and blocking, using this instead of synchronized and checking if size > 0
                request = requestQueue.take();
                request.logQueueEndTime();

                if (request != null) {
                    if (request.isSupportedGetRequest()) {
                        ProcessGetRequest(request);

                        synchronized (counter){
                            counter.logFromCompletedRequest(request, requestQueue.size());
                        }
                    }
                    else {
                        // Clear request buffer no matter whether it was correct because we need to read again!
                        // For response buffer we do not need to do this as it would not be read into
                        // Make sure always to clear request buffer before writing to memtier because of racing!
                        request.getRequestBuffer().clear();
                        request.getClientSocket().write(ByteBuffer.wrap(new String("Unsupported request\r\n").getBytes()));

                        synchronized (counter){
                            counter.logUnsupportedRequest();
                        }
                    }
                }
            } catch (IOException e) {
                Constants.MIDDLEWARE_LOGS_AND_EXCEPTIONS_SB.append("EXCEPTION: IO in WorkerThread.run()" + e.getMessage() + "\n");
                e.printStackTrace();
                System.exit(-1);
            } catch (InterruptedException e) {
                Constants.MIDDLEWARE_LOGS_AND_EXCEPTIONS_SB.append("EXCEPTION: Interrupted in WorkerThread.run()" + e.getMessage() + "\n");
                e.printStackTrace();
                System.exit(-1);
            }
        }
    }

    private void ProcessGetRequest(Request request) throws IOException {
        int serverToWaitResponse = roundRobin();

        writeToSocketFromFilledByteBuffer(serversChannels.get(serverToWaitResponse), request.getRequestBuffer());
        request.logServerStartTime();

        while (Constants.IS_MW_WORKING) {
            // Selector.select() blocks allowing for multiplexing since we have multiple connections (1 per server)
            // https://stackoverflow.com/questions/17615272/java-selector-is-asynchronous-or-non-blocking-architecture
            // This means that we will not drain CPU power unnecessary and line 120 is not problematic anymore
            serverSelector.select();
            Iterator<SelectionKey> keyIterator = serverSelector.selectedKeys().iterator();
            while (keyIterator.hasNext()) {
                SelectionKey key = (SelectionKey) keyIterator.next();
                keyIterator.remove();

                if (!key.isValid()) {
                    continue;
                }

                if (key.isAcceptable()) {
                    System.exit(-1);
                } else if (key.isReadable()) {
                    SocketChannel channel = (SocketChannel) key.channel();

                    int bytesRead = channel.read(responseBuffer);
                    if (bytesRead == -1) {
                        Constants.MIDDLEWARE_LOGS_AND_EXCEPTIONS_SB.append("LOG: Closed Memcached server: "
                                + ((SocketChannel) key.channel()).socket().getRemoteSocketAddress() + "in WorkerThread.ProcessGetRequest()\n");
                        ((SocketChannel) key.channel()).socket().close();

                        // If this happens we should really stop the experiment
                        System.exit(-1);
                    }

                    totalBytesRead += bytesRead;
                    int i;
                    for (i = 0; i < Constants.GET_REQUEST_END_LENGTH &&
                                responseBuffer.get(totalBytesRead - Constants.GET_REQUEST_END_LENGTH + i) == Constants.GET_REQUEST_END.get(i); i++);

                    if (i == Constants.GET_REQUEST_END_LENGTH){
                        request.logServerEndTime();
                        request.setSuccessful(totalBytesRead > 5);
                        request.setResponseLengthBytes(totalBytesRead);

                        writeToSocketFromFilledByteBuffer(request.getClientSocket(), responseBuffer);
                        request.logMiddlewareEndTime();

                        totalBytesRead = 0;
                        return;
                    }
                }
            }
        }
    }

    private void writeToSocketFromFilledByteBuffer(SocketChannel channel, ByteBuffer buffer) throws IOException {
        buffer.flip();
        while (buffer.hasRemaining()) {
            channel.write(buffer);
        }
        buffer.clear();
    }

    private int roundRobin() {
        return (loadBalance++ % serversChannels.size());
    }

    public Counter getCounter() {
        return counter;
    }
}