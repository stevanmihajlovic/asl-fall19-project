package ch.ethz;

import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.*;

public class RunMW {
    static String myIp = "127.0.0.1";
    static int myPort = 12345;
    static List<String> mcAddresses = Arrays.asList("127.0.0.1");
    static int numThreadsPTP = 32;
    static boolean readSharded = false;

    public static void main(String[] args) throws Exception {

        // -----------------------------------------------------------------------------
        // Prepare middleware to shutdown gracefully
        // -----------------------------------------------------------------------------

        installShutdownHook();
        //Thread.sleep(100000);

        // -----------------------------------------------------------------------------
        // Parse and prepare arguments
        // -----------------------------------------------------------------------------

        parseArguments(args);

        // -----------------------------------------------------------------------------
        // Start the Middleware
        // -----------------------------------------------------------------------------

        new MyMiddleware(myIp, myPort, mcAddresses, numThreadsPTP, readSharded).run();

    }

    private static void installShutdownHook() {
        Runtime.getRuntime().addShutdownHook(new Thread(){
            @Override
            public void run(){
                try {
                    // In order to ensure that Logger has another chance to write what ever happened in last second.
                    // This way last second will also be added to SB, multiplied by 1.5 to be safe
                    Thread.sleep((long) (Constants.STOPWATCH_TICK * 1.5));

                    Constants.IS_MW_WORKING = false;

                    PrintWriter summedWorkerThreads = new PrintWriter(Constants.SUMMED_WORKER_THREAD_COUNTERS_PATH);
                    summedWorkerThreads.write(Constants.SUMMED_WORKER_THREAD_COUNTERS_HEADER);
                    summedWorkerThreads.close();

                    // We do not need to lock access to SB because it will not be written by Logger for sure as IS_MW_WORKING cancelled and returned Logger
                    Files.write(Paths.get(Constants.SUMMED_WORKER_THREAD_COUNTERS_PATH),
                                Constants.SUMMED_WORKER_THREAD_COUNTERS_SB.toString().getBytes(), StandardOpenOption.APPEND);

                    PrintWriter middlewareLogsAndExceptions = new PrintWriter(Constants.MIDDLEWARE_LOGS_AND_EXCEPTIONS_PATH);
                    middlewareLogsAndExceptions.write("------------Middleware started!------------\n");
                    middlewareLogsAndExceptions.close();

                    Files.write(Paths.get(Constants.MIDDLEWARE_LOGS_AND_EXCEPTIONS_PATH),
                            Constants.MIDDLEWARE_LOGS_AND_EXCEPTIONS_SB.toString().getBytes(), StandardOpenOption.APPEND);
                } catch (Exception e) {
                    Constants.MIDDLEWARE_LOGS_AND_EXCEPTIONS_SB.append("EXCEPTION: general in RunMW.installShutDownHook()" + e.getMessage() + "\n");
                    e.printStackTrace();
                }
            }
        });
    }

    private static void parseArguments(String[] args) throws InterruptedException {
        Map<String, List<String>> params = new HashMap<>();

        List<String> options = null;
        for (int i = 0; i < args.length; i++) {
            final String a = args[i];

            if (a.charAt(0) == '-') {
                if (a.length() < 2) {
                    System.err.println("Error at argument " + a);
                    System.exit(1);
                }

                options = new ArrayList<String>();
                params.put(a.substring(1), options);
            } else if (options != null) {
                options.add(a);
            } else {
                System.err.println("Illegal parameter usage");
                System.exit(1);
            }
        }

        if (params.size() == 0) {
            printUsageWithError(null);
            System.exit(1);
        }

        if (params.get("l") != null)
            myIp = params.get("l").get(0);
        else {
            printUsageWithError("Provide this machine's external IP! (see ifconfig or your VM setup)");
            System.exit(1);
        }

        if (params.get("p") != null)
            myPort = Integer.parseInt(params.get("p").get(0));
        else {
            printUsageWithError("Provide the port, that the middleware listens to (e.g. 11212)!");
            System.exit(1);
        }

        if (params.get("m") != null) {
            mcAddresses = params.get("m");
        } else {
            printUsageWithError(
                    "Give at least one memcached backend server IP address and port (e.g. 123.11.11.10:11211)!");
            System.exit(1);
        }

        if (params.get("t") != null)
            numThreadsPTP = Integer.parseInt(params.get("t").get(0));
        else {
            printUsageWithError("Provide the number of threads for the threadpool!");
            System.exit(1);
        }

        if (params.get("s") != null)
            readSharded = Boolean.parseBoolean(params.get("s").get(0));
        else {
            printUsageWithError("Provide true/false to enable sharded reads!");
            System.exit(1);
        }

    }

    private static void printUsageWithError(String errorMessage) {
        System.err.println();
        System.err.println(
                "Usage: -l <MyIP> -p <MyListenPort> -t <NumberOfThreadsInPool> -s <readSharded> -m <MemcachedIP:Port> <MemcachedIP2:Port2> ...");
        if (errorMessage != null) {
            System.err.println();
            System.err.println("Error message: " + errorMessage);
        }

    }
}