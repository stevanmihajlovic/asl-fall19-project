package ch.ethz;

import java.nio.ByteBuffer;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;

public class Constants {
    public static final int MAX_KEY_SIZE = 250;
    public static final int NET_THREAD_BUFFER_SIZE = (int) (MAX_KEY_SIZE * 1.5);
    public static final int MAX_OBJECT_SIZE = 1024;
    public static final int RESPONSE_BUFFER_SIZE = (int) ((MAX_OBJECT_SIZE + MAX_KEY_SIZE) * 1.5);

    private static final String getRequestBegin = "get ";
    public static final ByteBuffer GET_REQUEST_BEGIN = ByteBuffer.wrap(getRequestBegin.getBytes());
    public static final int GET_REQUEST_BEGIN_LENGTH = getRequestBegin.length();

    private static final String getRequestEnd = "END\r\n";
    public static final ByteBuffer GET_REQUEST_END = ByteBuffer.wrap(getRequestEnd.getBytes());
    public static final int GET_REQUEST_END_LENGTH = getRequestEnd.length();

    // Do not introduce a warm up timer that will create a stopwatch timer, because I cannot see how long it really takes to warm-up
    public static final long STOPWATCH_TICK = 1000;
    // Not final, will be used as a counter
    public static AtomicLong STOPWATCH_TIME = new AtomicLong();

    // Not final, will be used to randomize load balance counter at start
    public static Random RANDOM = new Random();

    public static boolean IS_MW_WORKING = true;

    // Log files here
    public static final String SUMMED_WORKER_THREAD_COUNTERS_HEADER = "Second,Ops/sec,Hits/sec,Misses/sec,Latency,KB/sec," +
                                                                      "Request_Creation_Time,Request_Buffer_Time,MW_Writes_To_Memcached_Time," +
                                                                      "Memcached_Process_Time,MW_Writes_To_Memtier_Time,Queue_Size\n";
    public static final String SUMMED_WORKER_THREAD_COUNTERS_TEMPLATE = "%d,%.2f,%.2f,%.2f,%.3f,%.0f,%.3f,%.3f,%.3f,%.3f,%.3f,%.1f\n";
    // Put it in separate folders that will be named according to the VS-VC-WT and then put this inside
    public static final String SUMMED_WORKER_THREAD_COUNTERS_PATH = "Summed_Worker_Thread_Counters.csv";
    public static final StringBuffer SUMMED_WORKER_THREAD_COUNTERS_SB = new StringBuffer();

    public static final String MIDDLEWARE_LOGS_AND_EXCEPTIONS_PATH = "Middleware_Logs_And_Exceptions.txt";
    public static final StringBuffer MIDDLEWARE_LOGS_AND_EXCEPTIONS_SB = new StringBuffer();
}