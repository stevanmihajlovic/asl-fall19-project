package ch.ethz;

public class Counter {
    // All averages are calculated on request. I can output them per second like memtier or in end. To be decided!
    // Stopwatch time has been taken out to the Constants class instead

    // Important
    private long totalRequestHits;
    private long currentTimeframeRequestHits;

    // Important for hit/miss ratio
    private long totalRequestMiss;
    private long currentTimeframeRequestMiss;

    // Not important perhaps
    private long totalUnsupportedRequests;
    private long currentTimeframeUnsupportedRequests;

    // Not important perhaps
    private long totalBytesTransferred; // This will overflow
    private long currentTimeframeBytesTransferred;

    // Want to double check for myself only because of hashing
    private long totalHashingAndRequestConstructing;
    private long currentTimeframeHashingAndRequestConstructing;

    // Important
    private long totalTimeSpentInQueue;
    private long currentTimeframeTimeSpentInQueue;

    // Probably close to 0 and not important
    private long totalPreparingAndMemchachedWriting;
    private long currentTimeframePreparingAndMemchachedWriting;

    // Important
    private long totalServerProcessingTime;
    private long currentTimeframeServerProcessingTime;

    // Probably close to 0 and not important
    private long totalPreparingAndMemtierWriting;
    private long currentTimeframePreparingAndMemtierWriting;

    // Important
    private long totalResponseTime;
    private long currentTimeframeResponseTime;

    // Important
    private int totalQueueSize;
    private int currentTimeframeQueueSize;

    public void logUnsupportedRequest() {
        totalUnsupportedRequests++;
        currentTimeframeUnsupportedRequests++;
    }

    public void logFromCompletedRequest(Request request, int queueSize){
        if (!request.isSuccessful()){
            totalRequestMiss++;
            currentTimeframeRequestMiss++;
        }
        else {
            totalRequestHits++;
            currentTimeframeRequestHits++;

            totalBytesTransferred += (request.getRequestLengthBytes() + request.getResponseLengthBytes());
            currentTimeframeBytesTransferred += (request.getRequestLengthBytes() + request.getResponseLengthBytes());

            long hashingAndRequestConstructing = request.getQueueStartTime() - request.getMiddlewareStartTime();
            long timeSpentInQueue = request.getQueueEndTime() - request.getQueueStartTime();
            long preparingAndMemchachedWriting = request.getServerStartTime() - request.getQueueEndTime();
            long serverProcessingTime = request.getServerEndTime() - request.getServerStartTime();
            long preparingAndMemtierWriting = request.getMiddlewareEndTime() - request.getServerEndTime();
            long responseTime = request.getMiddlewareEndTime() - request.getMiddlewareStartTime();

            totalHashingAndRequestConstructing += hashingAndRequestConstructing;
            currentTimeframeHashingAndRequestConstructing += hashingAndRequestConstructing;

            totalTimeSpentInQueue += timeSpentInQueue;
            currentTimeframeTimeSpentInQueue += timeSpentInQueue;

            totalPreparingAndMemchachedWriting += preparingAndMemchachedWriting;
            currentTimeframePreparingAndMemchachedWriting += preparingAndMemchachedWriting;

            totalServerProcessingTime += serverProcessingTime;
            currentTimeframeServerProcessingTime += serverProcessingTime;

            totalPreparingAndMemtierWriting += preparingAndMemtierWriting;
            currentTimeframePreparingAndMemtierWriting += preparingAndMemtierWriting;

            totalResponseTime += responseTime;
            currentTimeframeResponseTime += responseTime;

            totalQueueSize += queueSize;
            currentTimeframeQueueSize += queueSize;
        }
    }

    public long getTotalRequestHits() {
        return totalRequestHits;
    }

    public long getCurrentTimeframeRequestHits() {
        return currentTimeframeRequestHits;
    }

    public void setCurrentTimeframeRequestHits(long currentTimeframeRequestHits) {
        this.currentTimeframeRequestHits = currentTimeframeRequestHits;
    }

    // Ops/sec
    public double getAverageRequestHits() {
        return totalRequestHits / 1000 / Constants.STOPWATCH_TIME.get();
    }

    public long getTotalRequestMiss() {
        return totalRequestMiss;
    }

    public long getCurrentTimeframeRequestMiss() {
        return currentTimeframeRequestMiss;
    }

    public void setCurrentTimeframeRequestMiss(long currentTimeframeRequestMiss) {
        this.currentTimeframeRequestMiss = currentTimeframeRequestMiss;
    }

    // Ops/sec
    public double getAverageRequestMiss() {
        return totalRequestMiss / 1000 / Constants.STOPWATCH_TIME.get();
    }

    public long getTotalUnsupportedRequests() {
        return totalUnsupportedRequests;
    }

    public long getCurrentTimeframeUnsupportedRequests() {
        return currentTimeframeUnsupportedRequests;
    }

    public void setCurrentTimeframeUnsupportedRequests(long currentTimeframeUnsupportedRequests) {
        this.currentTimeframeUnsupportedRequests = currentTimeframeUnsupportedRequests;
    }

    // Ops/sec
    public double getAverageUnsupportedRequests() {
        return totalUnsupportedRequests / 1000 / Constants.STOPWATCH_TIME.get();
    }

    public long getTotalBytesTransferred() {
        return totalBytesTransferred;
    }

    public long getCurrentTimeframeBytesTransferred() {
        return currentTimeframeBytesTransferred;
    }

    public void setCurrentTimeframeBytesTransferred(long currentTimeframeBytesTransferred) {
        this.currentTimeframeBytesTransferred = currentTimeframeBytesTransferred;
    }

    public double getAverageBytesTransferred() {
        return totalBytesTransferred / totalRequestHits;
    }

    public long getTotalHashingAndRequestConstructing() {
        return totalHashingAndRequestConstructing;
    }

    public long getCurrentTimeframeHashingAndRequestConstructing() {
        return currentTimeframeHashingAndRequestConstructing;
    }

    public void setCurrentTimeframeHashingAndRequestConstructing(long currentTimeframeHashingAndRequestConstructing) {
        this.currentTimeframeHashingAndRequestConstructing = currentTimeframeHashingAndRequestConstructing;
    }

    public double getAverageHashingAndRequestConstructing() {
        return totalHashingAndRequestConstructing / totalRequestHits;
    }

    public long getTotalTimeSpentInQueue() {
        return totalTimeSpentInQueue;
    }

    public long getCurrentTimeframeTimeSpentInQueue() {
        return currentTimeframeTimeSpentInQueue;
    }

    public void setCurrentTimeframeTimeSpentInQueue(long currentTimeframeTimeSpentInQueue) {
        this.currentTimeframeTimeSpentInQueue = currentTimeframeTimeSpentInQueue;
    }

    public double getAverageTimeSpentInQueue() {
        return totalTimeSpentInQueue / totalRequestHits;
    }

    public long getTotalPreparingAndMemchachedWriting() {
        return totalPreparingAndMemchachedWriting;
    }

    public long getCurrentTimeframePreparingAndMemchachedWriting() {
        return currentTimeframePreparingAndMemchachedWriting;
    }

    public void setCurrentTimeframePreparingAndMemchachedWriting(long currentTimeframePreparingAndMemchachedWriting) {
        this.currentTimeframePreparingAndMemchachedWriting = currentTimeframePreparingAndMemchachedWriting;
    }

    public double getAveragePreparingAndMemchachedWriting() {
        return totalPreparingAndMemchachedWriting / totalRequestHits;
    }

    public long getTotalServerProcessingTime() {
        return totalServerProcessingTime;
    }

    public long getCurrentTimeframeServerProcessingTime() {
        return currentTimeframeServerProcessingTime;
    }

    public void setCurrentTimeframeServerProcessingTime(long currentTimeframeServerProcessingTime) {
        this.currentTimeframeServerProcessingTime = currentTimeframeServerProcessingTime;
    }

    public double getAverageServerProcessingTime() {
        return totalServerProcessingTime / totalRequestHits;
    }

    public long getTotalPreparingAndMemtierWriting() {
        return totalPreparingAndMemtierWriting;
    }

    public long getCurrentTimeframePreparingAndMemtierWriting() {
        return currentTimeframePreparingAndMemtierWriting;
    }

    public void setCurrentTimeframePreparingAndMemtierWriting(long currentTimeframePreparingAndMemtierWriting) {
        this.currentTimeframePreparingAndMemtierWriting = currentTimeframePreparingAndMemtierWriting;
    }

    public double getAveragePreparingAndMemtierWriting() {
        return totalPreparingAndMemtierWriting / totalRequestHits;
    }

    public long getTotalResponseTime() {
        return totalResponseTime;
    }

    public long getCurrentTimeframeResponseTime() {
        return currentTimeframeResponseTime;
    }

    public void setCurrentTimeframeResponseTime(long currentTimeframeResponseTime) {
        this.currentTimeframeResponseTime = currentTimeframeResponseTime;
    }

    public double getAverageResponseTime() {
        return totalResponseTime / totalRequestHits;
    }

    public int getTotalQueueSize() {
        return totalQueueSize;
    }

    public int getCurrentTimeframeQueueSize() {
        return currentTimeframeQueueSize;
    }

    public void setCurrentTimeframeQueueSize(int currentTimeframeQueueSize) {
        this.currentTimeframeQueueSize = currentTimeframeQueueSize;
    }

    public double getAverageQueueSize(){
        return totalQueueSize / totalRequestHits;
    }
}
