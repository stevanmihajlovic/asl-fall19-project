package ch.ethz;

import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;

public class Request {
    private SocketChannel clientSocket;
    private boolean isSupportedGetRequest;
    // To avoid any conversions, pass ByteBuffer by reference and reuse it for writing to memcached server
    private ByteBuffer requestBuffer;
    private int requestLengthBytes;
    private int responseLengthBytes;

    // In case of get request, this is a key miss. Everything else is not supported
    private boolean isSuccessful;

    private long queueStartTime;
    private long queueEndTime;
    private long middlewareStartTime;
    private long middlewareEndTime;
    private long serverStartTime;
    private long serverEndTime;

    public Request(SocketChannel clSocket, ByteBuffer reqBody, int totalBytesRead){
        clientSocket = clSocket;
        requestBuffer = reqBody;
        requestLengthBytes = totalBytesRead;
        // Before ByteBuffer is used for writing from it after it was used for reading we need to .flip()
        // Before ByteBuffer is used for reading to it after it was used for writing we need to .clear()

        // We are parsing only get requests.
        // Multi-gets have the same keyword but more than 1 key, double-check for that
        // Everything else is not supported
        int j;
        for (j = 0; j < Constants.GET_REQUEST_BEGIN_LENGTH &&
                Constants.GET_REQUEST_BEGIN.get(j) == reqBody.get(j); j++);
        if (j == Constants.GET_REQUEST_BEGIN_LENGTH){
            int i;
            for (i = Constants.GET_REQUEST_BEGIN_LENGTH; i < totalBytesRead &&
                    reqBody.get(i) != ' '; i++);
            isSupportedGetRequest = i == totalBytesRead;
        }
        else {
            isSupportedGetRequest = false;
        }
    }


    public void logQueueStartTime() {
        queueStartTime = System.nanoTime();
    }

    public void logQueueEndTime() {
        queueEndTime = System.nanoTime();
    }

    public void logMiddlewareEndTime() {
        middlewareEndTime = System.nanoTime();
    }

    public void logServerStartTime() {
        serverStartTime = System.nanoTime();
    }

    public void logServerEndTime() {
        serverEndTime = System.nanoTime();
    }

    public boolean isSupportedGetRequest() {
        return isSupportedGetRequest;
    }

    public ByteBuffer getRequestBuffer() {
        return requestBuffer;
    }

    public boolean isSuccessful() {
        return isSuccessful;
    }

    public void setSuccessful(boolean successful) {
        isSuccessful = successful;
    }

    public SocketChannel getClientSocket() {
        return clientSocket;
    }

    public void setResponseLengthBytes(int respLenBytes){
        responseLengthBytes = respLenBytes;
    }

    public void logMiddlewareStartTime(Long firstTCP) {
        middlewareStartTime = firstTCP;
    }

    public long getQueueStartTime() {
        return queueStartTime;
    }

    public long getQueueEndTime() {
        return queueEndTime;
    }

    public long getMiddlewareStartTime() {
        return middlewareStartTime;
    }

    public long getMiddlewareEndTime() {
        return middlewareEndTime;
    }

    public long getServerStartTime() {
        return serverStartTime;
    }

    public long getServerEndTime() {
        return serverEndTime;
    }

    public int getRequestLengthBytes() {
        return requestLengthBytes;
    }

    public int getResponseLengthBytes() {
        return responseLengthBytes;
    }
}