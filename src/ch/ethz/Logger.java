package ch.ethz;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class Logger extends TimerTask {
    private Timer timer;
    private List<WorkerThread> workerThreads;

    public Logger(Timer timer, List<WorkerThread> workerThreads) {
        this.timer = timer;
        this.workerThreads = workerThreads;
    }


    @Override
    public void run() {
        if (!Constants.IS_MW_WORKING){
            timer.cancel();
            return;
        }
        double totalRequestHits = 0.0;
        double totalRequestMiss = 0.0;
        double totalUnsupportedRequests = 0.0;
        double totalBytesTransferred = 0.0;
        double totalHashingAndRequestConstructing = 0.0;
        double totalTimeSpentInQueue = 0.0;
        double totalPreparingAndMemchachedWriting = 0.0;
        double totalServerProcessingTime = 0.0;
        double totalPreparingAndMemtierWriting = 0.0;
        double totalResponseTime = 0.0;
        double totalQueueSize = 0.0;

        for (WorkerThread workerThread : workerThreads) {
            Counter workerThreadCounter = workerThread.getCounter();
            synchronized (workerThreadCounter){
                // Sum up everything
                totalRequestHits += workerThreadCounter.getCurrentTimeframeRequestHits();
                totalRequestMiss += workerThreadCounter.getCurrentTimeframeRequestMiss();
                totalUnsupportedRequests += workerThreadCounter.getCurrentTimeframeUnsupportedRequests();
                totalBytesTransferred += workerThreadCounter.getCurrentTimeframeBytesTransferred();
                totalHashingAndRequestConstructing += workerThreadCounter.getCurrentTimeframeHashingAndRequestConstructing();
                totalTimeSpentInQueue += workerThreadCounter.getCurrentTimeframeTimeSpentInQueue();
                totalPreparingAndMemchachedWriting += workerThreadCounter.getCurrentTimeframePreparingAndMemchachedWriting();
                totalServerProcessingTime += workerThreadCounter.getCurrentTimeframeServerProcessingTime();
                totalPreparingAndMemtierWriting += workerThreadCounter.getCurrentTimeframePreparingAndMemtierWriting();
                totalResponseTime += workerThreadCounter.getCurrentTimeframeResponseTime();
                totalQueueSize += workerThreadCounter.getCurrentTimeframeQueueSize();

                // Zero everything
                workerThreadCounter.setCurrentTimeframeRequestHits(0L);
                workerThreadCounter.setCurrentTimeframeRequestMiss(0L);
                workerThreadCounter.setCurrentTimeframeUnsupportedRequests(0L);
                workerThreadCounter.setCurrentTimeframeBytesTransferred(0L);
                workerThreadCounter.setCurrentTimeframeHashingAndRequestConstructing(0L);
                workerThreadCounter.setCurrentTimeframeTimeSpentInQueue(0L);
                workerThreadCounter.setCurrentTimeframePreparingAndMemchachedWriting(0L);
                workerThreadCounter.setCurrentTimeframeServerProcessingTime(0L);
                workerThreadCounter.setCurrentTimeframePreparingAndMemtierWriting(0L);
                workerThreadCounter.setCurrentTimeframeResponseTime(0L);
                workerThreadCounter.setCurrentTimeframeQueueSize(0);
            }
        }

        // For local testing important not to get divide by 0 because of exception and blocking of program
        if (totalRequestHits > 0) {
            // Divide everything
            Constants.SUMMED_WORKER_THREAD_COUNTERS_SB.append(String.format(Constants.SUMMED_WORKER_THREAD_COUNTERS_TEMPLATE,
                    Constants.STOPWATCH_TIME.get(), (totalRequestHits + totalRequestMiss) * 1000 / Constants.STOPWATCH_TICK,
                    totalRequestHits * 1000 / Constants.STOPWATCH_TICK, totalRequestMiss * 1000 / Constants.STOPWATCH_TICK,
                    totalResponseTime / 1000000.0 / totalRequestHits, totalBytesTransferred * 1000 / Constants.STOPWATCH_TICK / 1024,
                    totalHashingAndRequestConstructing / 1000000.0 / totalRequestHits, totalTimeSpentInQueue / 1000000.0 / totalRequestHits,
                    totalPreparingAndMemchachedWriting / 1000000.0 / totalRequestHits, totalServerProcessingTime / 1000000.0 / totalRequestHits,
                    totalPreparingAndMemtierWriting / 1000000.0 / totalRequestHits, totalQueueSize / totalRequestHits));
        }
        Constants.STOPWATCH_TIME.incrementAndGet();
    }
}
