package ch.ethz;

import java.nio.ByteBuffer;
import java.nio.channels.*;
import java.util.*;
import java.util.concurrent.LinkedBlockingQueue;
import java.net.*;
import java.io.*;

public class MyMiddleware {
    // In theory we are dealing with requests that can be split into several TCP packets. Therefore I have ByteBuffer per each client socket
    // In practice this should not happen as requests gets split if larger than 2KB and here we are dealing with 1K only. But better to be safe
    private HashMap<SocketChannel, ByteBuffer> leftOverMessagesMap = new HashMap<>();

    // Used to measure the time needed for hashing because I wanted to check how long does it take when Hashmaps are big
    // In theory with this I could also record think time by not resetting it at the end
    private HashMap<SocketChannel, Long> firstTCPPacketTimeStampMap = new HashMap<>();

    private InetSocketAddress listenAddress;
    private LinkedBlockingQueue<Request> requestQueue;

    public MyMiddleware(String myIp, int myPort, List<String> mcAddresses, int numThreadsPTP, boolean readSharded) {
        // Init MW Properties

        listenAddress = new InetSocketAddress(myIp, myPort);
        requestQueue = new LinkedBlockingQueue<>();

        List<WorkerThread> workerThreadsPool = new LinkedList<>();

        // InitWorkerThreads
        for (int i = 0; i < numThreadsPTP; i++) {
            workerThreadsPool.add(new WorkerThread(requestQueue, mcAddresses));
        }

        // Init memcached servers happens inside worker thread where each worker threads initializes connection to each server

        // Init Timeframe tick for logging
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new Logger(timer, workerThreadsPool), Constants.STOPWATCH_TICK, Constants.STOPWATCH_TICK);

        // Do not Init PrintWritters / FileWritters. Do it in batch inside Shutdown hook
    }

    void run() {
        try {
            // Non blocking multiclients
            Selector multiclientSelector = Selector.open();
            ServerSocketChannel serverSocketChannel = ServerSocketChannel.open();
            serverSocketChannel.configureBlocking(false);
            serverSocketChannel.socket().bind(listenAddress);
            serverSocketChannel.register(multiclientSelector, SelectionKey.OP_ACCEPT);

            Constants.MIDDLEWARE_LOGS_AND_EXCEPTIONS_SB.append("LOG: Waiting for connection in MyMiddleware.run()\n");

            while (Constants.IS_MW_WORKING) {
                multiclientSelector.select();
                Iterator keyIterator = multiclientSelector.selectedKeys().iterator();
                while (keyIterator.hasNext()) {
                    SelectionKey key = (SelectionKey) keyIterator.next();
                    keyIterator.remove();

                    if (!key.isValid()) {
                        continue;
                    }

                    if (key.isAcceptable()) {
                        SocketChannel clientSocket = serverSocketChannel.accept();
                        clientSocket.configureBlocking(false);
                        clientSocket.register(multiclientSelector, SelectionKey.OP_READ);
                        leftOverMessagesMap.put(clientSocket, ByteBuffer.allocate(Constants.NET_THREAD_BUFFER_SIZE));
                        firstTCPPacketTimeStampMap.put(clientSocket, new Long(0L));

                        Constants.MIDDLEWARE_LOGS_AND_EXCEPTIONS_SB.append("LOG: Accepted client: " +
                                clientSocket.socket().getRemoteSocketAddress() + "in MyMiddleware.run()\n");
                    } else if (key.isReadable()) {
                        SocketChannel clientSocket = (SocketChannel) key.channel();

                        // this is actually MW start time, when we first see TCP packet of request, not when we create it
                        if (firstTCPPacketTimeStampMap.get(clientSocket) == 0L) {
                            firstTCPPacketTimeStampMap.put(clientSocket, System.nanoTime());
                        }

                        ByteBuffer clientBuffer = leftOverMessagesMap.get(clientSocket);

                        // I assume that bytesRead is per TCP packet and not per total bytes in request
                        int bytesRead = clientSocket.read(clientBuffer);

                        // Maybe not needed
                        if (bytesRead == 0)
                            continue;
                        else if (bytesRead < 0)
                        {
                            Constants.MIDDLEWARE_LOGS_AND_EXCEPTIONS_SB.append("LOG: Closed client: "
                                    + clientSocket.socket().getRemoteSocketAddress() + "in MyMiddleware.run()\n");
                            clientSocket.close();
                            key.cancel();

                            // Losing a client is not so bad and we can continue
                            continue;
                        }

                        // Position of the buffer is accurate and I do not need totalBytesReadMap to memorize this in case of request being split over many TCP packet
                        int totalBytesRead = clientBuffer.position();

                        // In case fake set requests or multi-gets are sent, we will check it later on with the worker thread and discard if needed
                        // Since we are discarding everything until \n and we also expect get request to end with \n we can just create and enqueue it
                        // Then worker thread can pick request from queue and decide whether to discard or not (as in proj desc)
                        if(clientBuffer.get(totalBytesRead - 1) == '\n') {
                            Request request = new Request(clientSocket, clientBuffer, totalBytesRead);

                            request.logMiddlewareStartTime(firstTCPPacketTimeStampMap.get(clientSocket));
                            firstTCPPacketTimeStampMap.put(clientSocket, new Long(0L));

                            // Log before putting into queue because of racing!
                            request.logQueueStartTime();
                            requestQueue.put(request);
                        }
                    }
                }
            }

        } catch (ClosedChannelException e) {
            Constants.MIDDLEWARE_LOGS_AND_EXCEPTIONS_SB.append("EXCEPTION: ClosedChannel in MyMiddleware.run()" + e.getMessage() + "\n");
            e.printStackTrace();
        } catch (IOException e) {
            Constants.MIDDLEWARE_LOGS_AND_EXCEPTIONS_SB.append("EXCEPTION: IO in MyMiddleware.run()" + e.getMessage() + "\n");
            e.printStackTrace();
            System.exit(-1);
        } catch (InterruptedException e) {
            Constants.MIDDLEWARE_LOGS_AND_EXCEPTIONS_SB.append("EXCEPTION: Interrupted in MyMiddleware.run()" + e.getMessage() + "\n");
            e.printStackTrace();
            System.exit(-1);
        }
    }
}
