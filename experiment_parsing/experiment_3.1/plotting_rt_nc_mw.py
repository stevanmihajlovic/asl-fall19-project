import os
import re
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns


def parse_response_time(vs, vcpr, wtpm, run):
    # I will not have times folder in the final results. I will copy single one only to the repository
    root_path = "../../experiment_results/experiment_3.1/mw_"
    dir_paths = []
    for mw_num in range(1, number_of_middleware + 1):
        dir_paths.append(root_path + str(mw_num))

    j = 0
    average_response_time_in_single_repetition = 0
    # Match vs and vcpr
    for directory in dir_paths:
        for filename in os.listdir(directory):
            # Do not iterate over number_of_servers, regex will include them as well and then I will just average it all
            if re.match(r'Summed_Worker_Thread_Counters-vs_%d-vcpr_%d-wtpm_%d-rep_%d.csv' % (vs, vcpr, wtpm, run), filename):
                with open(os.path.join(directory, filename)) as file:
                    i = 0
                    for line in file:
                        i += 1
                        if mw_startup_time < i < duration - mw_shutdwown_time:
                            j += 1
                            average_response_time_in_single_repetition += float([x for x in line.split(",") if x != ""][4])

    # Regex includes all mw files so I should only average over number of rows
    return average_response_time_in_single_repetition / j


if __name__ == '__main__':
    # From the configuration of the experiment
    number_of_servers = 1
    number_of_client_machines = 3
    instances_of_memtier_per_machine = 1
    threads_per_memtier_instance = 2
    virtual_clients_per_thread = [4, 8, 16, 32]
    value_size = [64, 256, 512, 1024]
    number_of_middleware = 1
    worker_threads_per_middleware = [8, 32, 64]
    repetitions = 4
    duration = 70
    # We will cutoff first 5 and last 5 in data processing
    mw_startup_time = 5
    mw_shutdwown_time = 5

    for wtpm in worker_threads_per_middleware:

        # Instantiating df
        x_axis = []
        x_axis_generated = False
        df = pd.DataFrame()
        # markers=True, dashes=False for some reason do not work and I will add helper df
        x_axis_scatterplot = []
        dfs = pd.DataFrame()

        # Instantiating plot
        sns.set()  # This sets the style to the seaborn default (gray background with white grid on)
        fig, ax = plt.subplots()  # create your figure and ax objects

        for vs in value_size:
            # Re-instantiate and use same array for each vs
            y_axis = []
            for vcpr in virtual_clients_per_thread:
                # Maybe replace the for loops in the experiment script as well
                # Actually maybe not since we can run on several clients at the same time (parallel) which will save us time

                # We create x axis only once as it will have same Total number of clients for each vs
                if not x_axis_generated:
                    total_number_of_clients = vcpr * number_of_client_machines * instances_of_memtier_per_machine * threads_per_memtier_instance
                    for run in range(2, repetitions + 1):
                        x_axis.append(total_number_of_clients)
                    x_axis_scatterplot.append(total_number_of_clients)

                # Fill the y axis with all repetitions at a time, do this per vcpr
                average_response_time_in_all_repetition_list = []
                for run in range(2, repetitions + 1):
                    average_response_time_in_all_repetition_list.append(parse_response_time(vs, vcpr, wtpm, run))
                y_axis += average_response_time_in_all_repetition_list

            # This should happen only once, we use same X axis for all the vs series
            if not x_axis_generated:
                x_axis_generated = True
                df["Total number of clients"] = x_axis
                dfs["Total number of clients"] = x_axis_scatterplot

            df[str(vs)] = y_axis

            ax.set_ylim(bottom=0, top=max(y_axis)*1.1)
            ax.set_xlim(left=0, right=max(x_axis)*1.1)
            # Plot lines
            sns.lineplot('Total number of clients', str(vs), ci="sd", data=df, ax=ax, label=(str(vs)+' bytes'))

            # markers=True, dashes=False for some reason do not work and I will add helper df
            y_axis_scatterplot = []
            for i in range(0, len(y_axis), repetitions - 1):
                y_axis_scatterplot_single = []
                for j in range(0, repetitions - 1):
                    y_axis_scatterplot_single.append(y_axis[i+j])
                y_axis_scatterplot.append(np.mean(y_axis_scatterplot_single))
            dfs[str(vs)] = y_axis_scatterplot

            # Plot dots
            sns.scatterplot('Total number of clients', str(vs), data=dfs)
        dfs.to_csv('mw-wtpm_' + str(wtpm) + '_rt.csv', header=True)

        # Finalize the plot
        plt.legend()
        ax.set_ylabel('Response time in ms')
        fig.show()
        figname = 'mw-wtpm_' + str(wtpm) + '_rt_nc.png'
        fig.savefig(figname)

