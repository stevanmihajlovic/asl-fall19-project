import os
import re
import numpy as np
import csv


def parse_column(root_path, vs, vcpr, wtpm, path, column):
    dir_paths = []
    dir_paths.append(root_path + path + "/dstat")

    starting_repetition = 1
    if (root_path == "../experiment_results/experiment_3.1/" and vs == 64 and vcpr == 4 and wtpm == 8):
        starting_repetition = 2

    average_column_value_in_all_repetition_list = []

    for run in range(starting_repetition, repetitions + 1):
        j = 0
        average_column_value_in_single_repetition = 0
        start_counting = False

        for directory in dir_paths:
            for filename in os.listdir(directory):
                if re.match(r'vs_%d-vcpr_%d-wtpm_%d-rep_%d.csv' % (vs, vcpr, wtpm, run),
                            filename):
                    with open(os.path.join(directory, filename)) as file:
                        i = 0
                        for line in file:
                            if start_counting:
                                i += 1
                            if line.startswith('"usr"'):
                                start_counting = True

                            if mw_startup_time < i < duration - mw_shutdwown_time:
                                j += 1
                                average_column_value_in_single_repetition += float(
                                    [x for x in line.split(",") if x != ""][column])

        # Regex includes only current mw or server files so I should only average over number of rows
        average_column_value_in_all_repetition_list.append(average_column_value_in_single_repetition / j)

    return "{:.3f}".format(np.mean(average_column_value_in_all_repetition_list))


if __name__ == '__main__':
    # From the configuration of the experiment
    number_of_servers = 1
    number_of_client_machines = 3
    instances_of_memtier_per_machine = 1
    threads_per_memtier_instance = 2
    virtual_clients_per_thread = [4, 8, 16, 32]
    value_size = [64, 256, 512, 1024]
    worker_threads_per_middleware = [8, 32, 64]
    repetitions = 4
    duration = 16
    # We will cutoff first 1 and last 1 in data processing
    mw_startup_time = 2
    mw_shutdwown_time = 1
    experiments_root_paths = ["../experiment_results/experiment_3.1/", "../experiment_results/experiment_3.2/",
                              "../experiment_results/experiment_3.3/", "../experiment_results/experiment_3.4/"]
    experiments_number_of_middleware = [1, 1, 2, 2]
    experiments_number_of_servers = [1, 3, 1, 3]

    header = ["experiment", "wtpm", "vs", "nc", "machine", "usr", "sys", "idl", "wai", "hiq", "siq", "read", "writ",
              "recv", "send", "in", "out", "int", "csw", "cpu"]
    lines = [header]
    for root_path, number_of_middleware, number_of_server in zip(experiments_root_paths,
                                                                 experiments_number_of_middleware,
                                                                 experiments_number_of_servers):
        for wtpm in worker_threads_per_middleware:
            for vs in value_size:
                for vcpr in virtual_clients_per_thread:
                    for num_mw in range(1, number_of_middleware + 1):
                        path = "mw_" + str(num_mw)
                        row = [root_path, wtpm, vs,  # Product below is always the same for all section 3 experiments
                               vcpr * number_of_client_machines * instances_of_memtier_per_machine * threads_per_memtier_instance, path]

                        for column in range(0, 14):
                            row.append(parse_column(root_path, vs, vcpr, wtpm, path, column))

                        row.append("{:.0f}".format(100 - float(row[7])))
                        lines.append(row)
                    for num_sv in range(1, number_of_server + 1):
                        path = "server_" + str(num_sv)
                        row = [root_path, wtpm, vs,  # Product below is always the same for all section 3 experiments
                               vcpr * number_of_client_machines * instances_of_memtier_per_machine * threads_per_memtier_instance, path]

                        for column in range(0, 14):
                            row.append(parse_column(root_path, vs, vcpr, wtpm, path, column))

                        row.append("{:.0f}".format(100 - float(row[7])))
                        lines.append(row)

    with open('average_dstat_all_section_3_experiments.csv', 'w') as csvFile:
        writer = csv.writer(csvFile)
        writer.writerows(lines)

    csvFile.close()
