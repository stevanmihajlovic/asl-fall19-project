import os
import re
import numpy as np
import csv


def parse_column(root_path, vs, vcpr, wtpm, number_of_middleware, column):
    dir_paths = []
    for mw_num in range(1, number_of_middleware + 1):
        dir_paths.append(root_path + str(mw_num))

    average_column_value_in_all_repetition_list = []

    j = 0
    average_column_value_in_single_repetition = 0

    for directory in dir_paths:
        for filename in os.listdir(directory):
            if re.match(r'Summed_Worker_Thread_Counters-vs_%d-vcpr_%d-wtpm_%d-rep_%d.csv' % (vs, vcpr, wtpm, run),
                        filename):
                with open(os.path.join(directory, filename)) as file:
                    i = 0
                    for line in file:
                        i += 1
                        if mw_startup_time < i < duration - mw_shutdwown_time:
                            j += 1
                            average_column_value_in_single_repetition += float(
                                [x for x in line.split(",") if x != ""][column])

    # Regex includes all mw files so I should only average over number of rows
    # I need to distinguish whether I am dealing with time because it averages different than throughput for example
    if column in [4, 6, 7, 8, 9, 10]:
        average_column_value_in_all_repetition_list.append(average_column_value_in_single_repetition / j)
    else:
        average_column_value_in_all_repetition_list.append(
            average_column_value_in_single_repetition * number_of_middleware / j)

    if column in [4, 6, 7, 8, 9, 10]:
        return "{:.3f}".format(np.mean(average_column_value_in_all_repetition_list))

    if column == 11:
        return "{:.1f}".format(np.mean(average_column_value_in_all_repetition_list))

    if column == 5:
        return "{:.0f}".format(np.mean(average_column_value_in_all_repetition_list))

    return "{:.2f}".format(np.mean(average_column_value_in_all_repetition_list))


if __name__ == '__main__':
    # From the configuration of the experiment
    number_of_servers = 1
    number_of_client_machines = 3
    instances_of_memtier_per_machine = 1
    threads_per_memtier_instance = 2
    virtual_clients_per_thread = [32]
    value_size = [256]
    worker_threads_per_middleware = [8, 32]
    repetitions = 4
    duration = 70
    # We will cutoff first 5 and last 5 in data processing
    mw_startup_time = 5
    mw_shutdwown_time = 5
    experiments_root_paths = ["../experiment_results/experiment_3.1/mw_", "../experiment_results/experiment_3.2/mw_",
                              "../experiment_results/experiment_3.3/mw_", "../experiment_results/experiment_3.4/mw_"]
    experiments_number_of_middleware = [1, 1, 2, 2]

    lines = []
    for root_path, number_of_middleware in zip(experiments_root_paths, experiments_number_of_middleware):
        for wtpm in worker_threads_per_middleware:
            for vs in value_size:
                for vcpr in virtual_clients_per_thread:
                    for run in range(1, repetitions + 1):

                        row = [root_path, wtpm, vs, run,#Product below is always the same for all section 3 experiments
                               vcpr * number_of_client_machines * instances_of_memtier_per_machine * threads_per_memtier_instance]

                        for column in range(1, 12):
                            row.append(parse_column(root_path, vs, vcpr, wtpm, number_of_middleware, column))

                        lines.append(row)

    with open('create_2k_data.csv', 'w') as csvFile:
        writer = csv.writer(csvFile)
        writer.writerows(lines)

    csvFile.close()
