import os
import re
import numpy as np
import csv


def parse_rt_csv(regex_in):
    for filename in os.listdir(directory):
        if re.match(re.compile(regex_in), filename):
            rows = []
            with open(os.path.join(directory, filename)) as file:
                reader = csv.reader(file)
                for line in reader:
                    rows.append(line)
            file.close()

            newrow = []
            for j in range(0, 4):
                for i in range(0, 4):
                    newrow.append(float(rows[i + 1][j + 2]))
            return np.around(newrow, decimals=2)


def parse_tp_csv(regex_in):
    for filename in os.listdir(directory):
        if re.match(re.compile(regex_in), filename):
            rows = []
            with open(os.path.join(directory, filename)) as file:
                reader = csv.reader(file)
                for line in reader:
                    rows.append(line)
            file.close()

            newrow = []
            # Because of setting max for TP in reverse order, it is saved in file in reverse order as well
            for j in range(-1, -5, -1):
                for i in range(0, 4):
                    newrow.append(float(rows[i + 1][j]))
            return np.around(newrow, decimals=2)


if __name__ == '__main__':
    experiments_root_paths = ["./experiment_3.1/", "./experiment_3.2/",
                              "./experiment_3.3/", "./experiment_3.4/"]
    worker_threads_per_middleware = [8, 32, 64]
    for directory in experiments_root_paths:
        for wtpm in worker_threads_per_middleware:
            all_rows = []
            all_rows.append([24.0, 48.0, 96.0, 192.0,
                             24.0, 48.0, 96.0, 192.0,
                             24.0, 48.0, 96.0, 192.0,
                             24.0, 48.0, 96.0, 192.0])
            all_rows.append(parse_tp_csv('mw-wtpm_%d_tp.csv' % wtpm))
            all_rows.append(parse_rt_csv('mw-wtpm_%d_rt.csv' % wtpm))
            all_rows.append(np.around(np.divide(all_rows[0], np.multiply(all_rows[1], all_rows[2])), decimals=2))
            all_rows.append(parse_rt_csv('memtier_clients-wtpm_%d_rt.csv' % wtpm))
            all_rows.append(np.around(np.divide(all_rows[0], np.multiply(all_rows[1], all_rows[4])), decimals=2))

            csvFile = open(os.path.join(directory, 'interactive_law-wtpm_%d' % wtpm), "w")
            writer = csv.writer(csvFile)
            writer.writerows(all_rows)
            csvFile.close()