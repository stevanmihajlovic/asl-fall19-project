import os
import re
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns


def parse_response_time(vs, vcpr, run):
    # I will not have times folder in the final results. I will copy single one only to the repository
    root_path = "../../experiment_results/experiment_2.1/client_"
    dir_paths = []
    for cl_num in range(1, number_of_client_machines + 1):
        dir_paths.append(root_path + str(cl_num))

    average_response_time_in_single_repetition = 0
    # Match vs and vcpr
    for directory in dir_paths:
        for filename in os.listdir(directory):
            # Do not iterate over number_of_servers, regex will include them as well and then I will just average it all
            if re.match(r'vs_%d-vcpr_%d-server_.-rep_%d' % (vs, vcpr, run), filename):
                with open(os.path.join(directory, filename)) as file:
                    for line in file:
                        if line.startswith('Gets'):
                            average_response_time_in_single_repetition += float([x for x in line.split(" ") if x != ""][4])

    return average_response_time_in_single_repetition / (number_of_client_machines * number_of_servers)


if __name__ == '__main__':
    # From the configuration of the experiment
    number_of_servers = 1
    number_of_client_machines = 3
    instances_of_memtier_per_machine = 1
    threads_per_memtier_instance = 3
    virtual_clients_per_thread = [4, 8, 16, 32]
    value_size = [64, 256, 512, 1024]
    repetitions = 4

    # Instantiating df
    x_axis = []
    x_axis_generated = False
    df = pd.DataFrame()
    # markers=True, dashes=False for some reason do not work and I will add helper df
    x_axis_scatterplot = []
    dfs = pd.DataFrame()

    # Instantiating plot
    sns.set()  # This sets the style to the seaborn default (gray background with white grid on)
    fig, ax = plt.subplots()  # create your figure and ax objects

    for vcpr in virtual_clients_per_thread:
        # Re-instantiate and use same array for each vcpr
        y_axis = []
        for vs in value_size:
            # Maybe replace the for loops in the experiment script as well
            # Actually maybe not since we can run on several clients at the same time (parallel) which will save us time

            # We create x axis only once as it will have same Total number of clients for each vcpr
            if not x_axis_generated:
                for run in range(1, repetitions + 1):
                    x_axis.append(vs)
                x_axis_scatterplot.append(vs)

            # Fill the y axis with all repetitions at a time, do this per vcpr
            average_response_time_in_all_repetition_list = []
            for run in range(1, repetitions + 1):
                average_response_time_in_all_repetition_list.append(parse_response_time(vs, vcpr, run))
            y_axis += average_response_time_in_all_repetition_list

        # This should happen only once, we use same X axis for all the vcpr series
        if not x_axis_generated:
            x_axis_generated = True
            df["Value size in bytes"] = x_axis
            dfs["Value size in bytes"] = x_axis_scatterplot

        df[str(vcpr)] = y_axis

        ax.set_ylim(bottom=0, top=max(y_axis)*1.1)
        ax.set_xlim(left=0, right=max(x_axis)*1.1)
        # Plot lines
        total_number_of_clients = vcpr * number_of_client_machines * instances_of_memtier_per_machine * threads_per_memtier_instance
        sns.lineplot('Value size in bytes', str(vcpr), ci="sd", data=df, ax=ax, label=(str(total_number_of_clients)+' clients'))

        # markers=True, dashes=False for some reason do not work and I will add helper df
        y_axis_scatterplot = []
        for i in range(0, len(y_axis), repetitions):
            y_axis_scatterplot_single = []
            for j in range(0, repetitions):
                y_axis_scatterplot_single.append(y_axis[i+j])
            y_axis_scatterplot.append(np.mean(y_axis_scatterplot_single))
        dfs[str(vcpr)] = y_axis_scatterplot

        # Plot dots
        sns.scatterplot('Value size in bytes', str(vcpr), data=dfs)

    # Finalize the plot
    plt.legend()
    ax.set_ylabel('Response time in ms')
    fig.show()
    fig.savefig('memtier-clients_rt_vs.png')

