pkg load queueing;
load network_of_queues_in.csv
V=[1 0.5 0.5 0.5 0.5];
Z=0;
for i=1:rows(network_of_queues_in)
  N=network_of_queues_in(i,2);
  m=[0 1 1 network_of_queues_in(i,1) network_of_queues_in(i,1)];
  S=[network_of_queues_in(i,3) network_of_queues_in(i,4) network_of_queues_in(i,4) network_of_queues_in(i,5) network_of_queues_in(i,5)];
  [U,R,Q,X,G] = qncsmva(N,S,V,m);
  X_s = X(1)/V(1); # System throughput
  R_s = dot(R,V); # System response time
  printf("\t    Util      Qlen     RespT      Tput\n");
  printf("\t--------  --------  --------  --------\n");
  for k=1:length(S)
    printf("Dev%d\t%8.4f  %8.4f  %8.4f  %8.4f\n", k, U(k), Q(k), R(k), X(k) );
  endfor
  printf("\nSystem\t          %8.4f  %8.4f  %8.4f\n\n", N-X_s*Z, R_s, X_s );
endfor