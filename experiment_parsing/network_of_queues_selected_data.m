pkg load queueing;
load network_of_queues_in.csv
V=[1 0.5 0.5 0.5 0.5];
Z=0;
printf("U_D\trho_N\trho_W\tRT_N\tRT_W\tRT_M\tTP_M\n");
for i=1:rows(network_of_queues_in)
  N=network_of_queues_in(i,2);
  m=[0 1 1 network_of_queues_in(i,1) network_of_queues_in(i,1)];
  S=[network_of_queues_in(i,3) network_of_queues_in(i,4) network_of_queues_in(i,4) network_of_queues_in(i,5) network_of_queues_in(i,5)];
  [U,R,Q,X,G] = qncsmva(N,S,V,m);
  X_s = X(1)/V(1); # System throughput
  R_s = dot(R,V); # System response time
  printf("%8.4f\t%8.4f\t%8.4f\t%8.4f\t%8.4f\t%8.4f\t%8.4f\n", U(1), U(2), U(4), R(2), R(4), R_s, X_s);
endfor