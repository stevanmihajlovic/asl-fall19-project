import os
import re
import numpy as np
import csv


def parse_column(root_path, vs, vcpr, number_of_servers, column):
    dir_paths = []
    for cl_num in range(1, number_of_client_machines + 1):
        dir_paths.append(root_path + str(cl_num))

    average_column_value_in_all_repetition_list = []

    for run in range(1, repetitions + 1):
        average_column_value_in_single_repetition = 0

        for directory in dir_paths:
            for filename in os.listdir(directory):
                # Do not iterate over number_of_servers, regex will include them as well and then I will just average it all
                if re.match(r'vs_%d-vcpr_%d-server_.-rep_%d' % (vs, vcpr, run), filename):
                    with open(os.path.join(directory, filename)) as file:
                        for line in file:
                            if line.startswith('Gets'):
                                average_column_value_in_single_repetition += float(
                                    [x for x in line.split(" ") if x != ""][column])

        # I need to distinguish whether I am dealing with time because it averages different than throughput for example
        if column == 4:
            average_column_value_in_all_repetition_list.append(average_column_value_in_single_repetition / (number_of_client_machines * number_of_servers))
        else:
            average_column_value_in_all_repetition_list.append(average_column_value_in_single_repetition)

    if column == 4:
        return "{:.3f}".format(np.mean(average_column_value_in_all_repetition_list))

    if column == 5:
        return "{:.0f}".format(np.mean(average_column_value_in_all_repetition_list))

    return "{:.2f}".format(np.mean(average_column_value_in_all_repetition_list))


if __name__ == '__main__':
    # From the configuration of the experiment
    number_of_servers = 1
    number_of_client_machines = 3
    instances_of_memtier_per_machine = 1
    threads_per_memtier_instance = 3
    virtual_clients_per_thread = [4, 8, 16, 32]
    value_size = [64, 256, 512, 1024]
    repetitions = 4

    experiments_root_paths = ["../experiment_results/experiment_2.1/client_", "../experiment_results/experiment_2.2/client_"]
    experiments_number_of_servers = [1, 3]

    header = ["experiment", "vs", "nc", "ops/s", "hits/s", "miss/s", "rt(ms)", "KB/s"]
    lines = [header]
    for root_path, number_of_servers in zip(experiments_root_paths, experiments_number_of_servers):
        for vs in value_size:
            for vcpr in virtual_clients_per_thread:
                row = [root_path, vs,#Product below is always the same for all section 3 experiments
                       vcpr * number_of_client_machines * instances_of_memtier_per_machine * threads_per_memtier_instance]

                for column in range(1, 6):
                    row.append(parse_column(root_path, vs, vcpr, number_of_servers, column))

                lines.append(row)

    with open('average_memtier_all_section_2_experiments.csv', 'w') as csvFile:
        writer = csv.writer(csvFile)
        writer.writerows(lines)

    csvFile.close()
